minikube start --vm-driver=hyperkit -v 7
eval $(minikube docker-env)
docker images
REPOSITORY                                                       TAG                                        IMAGE ID            CREATED             SIZE
bitnami/redis                                                    4.0.11-debian-9                            708c65972174        21 hours ago        90.9MB
bitnami/redis                                                    <none>                                     56fd89cbbdf5        45 hours ago        90.9MB
registry.gitlab.com/gitlab-org/build/cng/gitlab-task-runner-ee   v11.2.3                                    565add3f3789        4 days ago          1.33GB
registry.gitlab.com/gitlab-org/build/cng/gitlab-sidekiq-ee       v11.2.3                                    ac4d6722e88f        4 days ago          1.3GB
registry.gitlab.com/gitlab-org/build/cng/gitlab-rails-ee         v11.2.3                                    8d24629b2ea5        4 days ago          1.3GB
registry.gitlab.com/gitlab-org/build/cng/gitlab-shell            v8.1.1                                     03cc8a973629        4 days ago          591MB
postgres                                                         9.6                                        c0b5c11b5690        4 days ago          229MB
gcr.io/kubernetes-helm/tiller                                    v2.10.0                                    0cccc6576d01        4 weeks ago         69MB
registry.gitlab.com/gitlab-org/build/cng/alpine-certificates     20171114-r3                                2742a6c474b8        4 weeks ago         4.97MB
busybox                                                          latest                                     e1ddd7948a1c        6 weeks ago         1.16MB
minio/mc                                                         RELEASE.2018-07-13T00-53-22Z               ee62869f5c8d        2 months ago        14.2MB
quay.io/jetstack/cert-manager-controller                         v0.4.0                                     855b09bf59d2        2 months ago        51.8MB
registry.gitlab.com/gitlab-org/build/cng/kubectl                 1f8690f03f7aeef27e727396927ab3cc96ac89e7   95c3f2b609cf        3 months ago        80.8MB
quay.io/kubernetes-ingress-controller/nginx-ingress-controller   0.15.0                                     c46bc3e1b53c        4 months ago        265MB
k8s.gcr.io/kube-proxy-amd64                                      v1.10.0                                    bfc21aadc7d3        5 months ago        97MB
k8s.gcr.io/kube-apiserver-amd64                                  v1.10.0                                    af20925d51a3        5 months ago        225MB
k8s.gcr.io/kube-controller-manager-amd64                         v1.10.0                                    ad86dbed1555        5 months ago        148MB
k8s.gcr.io/kube-scheduler-amd64                                  v1.10.0                                    704ba848e69a        5 months ago        50.4MB
prom/prometheus                                                  v2.2.1                                     cc866859f8df        6 months ago        113MB
k8s.gcr.io/etcd-amd64                                            3.1.12                                     52920ad46f5b        6 months ago        193MB
k8s.gcr.io/kube-addon-manager                                    v8.6                                       9c16409588eb        6 months ago        78.4MB
k8s.gcr.io/k8s-dns-dnsmasq-nanny-amd64                           1.14.8                                     c2ce1ffb51ed        8 months ago        41MB
k8s.gcr.io/k8s-dns-sidecar-amd64                                 1.14.8                                     6f7f2dc7fab5        8 months ago        42.2MB
k8s.gcr.io/k8s-dns-kube-dns-amd64                                1.14.8                                     80cc5ea4b547        8 months ago        50.5MB
minio/minio                                                      RELEASE.2017-12-28T01-21-00Z               8cf453f32acf        8 months ago        28.8MB
gitlab/gitlab-runner                                             alpine-v10.3.0                             0282be6186b1        8 months ago        105MB
k8s.gcr.io/pause-amd64                                           3.1                                        da86e6ba6ca1        8 months ago        742kB
k8s.gcr.io/kubernetes-dashboard-amd64                            v1.8.1                                     e94d2f21bc0c        9 months ago        121MB
gcr.io/k8s-minikube/storage-provisioner                          v1.8.1                                     4689081edb10        10 months ago       80.8MB
bitnami/redis                                                    3.2.9-r2                                   40856dba0c5d        15 months ago       119MB
k8s.gcr.io/defaultbackend                                        1.3                                        a70ad572a50f        18 months ago       3.62MB
mysql                                                            5.7.14                                     4b3b6b994512        2 years ago         385MB
jimmidyson/configmap-reload                                      v0.1                                       b70d7dba98e6        2 years ago         4.78MB
busybox                                                          1.25.0                                     2b8fd9751c4c        2 years ago         1.09MB
k8s.gcr.io/echoserver                                            1.4                                        a90209bb39e3        2 years ago         140MB
kubectl run hello-minikube --image=k8s.gcr.io/echoserver:1.4 --port=8080
deployment "hello-minikube" created
andywang@andywang-mbp  ~/sh  kubectl get pod
NAME                             READY     STATUS    RESTARTS   AGE
hello-minikube-6c47c66d8-gvtpm   1/1       Running   0          41s
 andywang@andywang-mbp  ~/sh  kubectl expose deployment hello-minikube --type=NodePort
service "hello-minikube" exposed
 ✘ andywang@andywang-mbp  ~/sh  curl $(minikube service hello-minikube --url)
CLIENT VALUES:
client_address=172.17.0.1
command=GET
real path=/
query=nil
request_version=1.1
request_uri=http://192.168.64.3:8080/

SERVER VALUES:
server_version=nginx: 1.10.0 - lua: 10001

HEADERS RECEIVED:
accept=*/*
host=192.168.64.3:31850
user-agent=curl/7.54.0
BODY:
-no body in request-%
andywang@andywang-mbp  ~/sh  kubectl delete service hello-minikube
service "hello-minikube" deleted
 andywang@andywang-mbp  ~/sh  kubectl delete deployment hello-minikube
deployment "hello-minikube" deleted
 andywang@andywang-mbp  ~/sh  kubectl get pod
NAME                             READY     STATUS        RESTARTS   AGE
hello-minikube-6c47c66d8-gvtpm   0/1       Terminating   0          6m
 andywang@andywang-mbp  ~/sh  kubectl get pod
No resources found.
 andywang@andywang-mbp  ~/sh  kubectl get service
NAME         TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
kubernetes   ClusterIP   10.96.0.1    <none>        443/TCP   1d
---
